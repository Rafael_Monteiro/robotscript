export default class BasicOperations {
    private static readonly ZERO:number = 0;
    public static sum (...args:number[]) {
        return args.reduce((prev:number, curr:number) => {
            return prev+curr
        });
    }

    public static subtract(...args: number[]){
        return args.reduce((prev:number, curr:number) => {
            return prev-curr;
        });
    }

    public static multiply(...args: number[]) {
        return args.reduce((prev:number, curr: number) => {
            return prev*curr;
        });
    }

    public static divide(...args: number[]) {
        if (args.includes(this.ZERO) && args.indexOf(this.ZERO) !== this.ZERO) {
            throw new Error(`Division by zero encountered at index [${args.indexOf(this.ZERO)}]`);
        }
        return args.reduce((prev:number, curr:number) => {
            return prev/curr;
        })
    }
}