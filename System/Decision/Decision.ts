import LogicalOperator from "./LogicalOperator";

export default class Decide {

    public static ternary(condition: boolean, operation: () => any, orElse: () => any) {
        if (condition) {
            return operation();
        } else {
            return orElse();
        }
    }

    public static if(condition: boolean, action: () => any): LogicalOperator {
        if (condition) {
            action();
        }
        return new LogicalOperator(condition);
    }
}