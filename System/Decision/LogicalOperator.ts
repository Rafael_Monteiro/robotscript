export default class LogicalOperator {
    posteriorCondition:boolean = false;

    constructor(posteriorCondition: boolean) {
        this.posteriorCondition = posteriorCondition;
    }

    public elif(condition: boolean, operation: () => any) {
        if (!this.posteriorCondition && condition) {
            operation();
        }
        return new LogicalOperator(condition);
    }

    public else(operation: () => any) {
        if (!this.posteriorCondition) {
            return operation();
        }
    }
}