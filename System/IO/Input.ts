import * as LineReader from "readline";
export default class Input {
    private static reader: LineReader.Interface = LineReader.createInterface({
        input:process.stdin,
        output: process.stdout
    });

    /**
     * Le o proximo valor digitado pelo usuario
     * @param args texto que sera inserido na tela
     * @returns uma promise com texto digitado pelo usuario
     */
    public static readLine(...args : Array<string>): Promise<string> {
        return new Promise<string>(async(resolve) => {
            this.reader.question(args.join(), async(ans: string) => {
                this.reader.close();
                return resolve(ans);
            });
        });
    } 
}