export default class Output {
    public static write(...args : Array<any>) {
        console.log(`${args.join()}`);
    }

    public static writeF(text:string, maskValues: any) {
        console.log(text.split('{}').join(maskValues.toString()));
    }
}