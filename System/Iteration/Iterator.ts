export default class Iterator {
    
    public static loop(action: (arg?:any) => any, iterations: number) {
        for (let i = 0; i < iterations; i++) {
            action();
        }
    }
}