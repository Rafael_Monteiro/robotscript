export default class ListHandler {
    public static order(listToOrder: Array<number>, reverse?:boolean) {
        return listToOrder.sort((prev, next) => {
           if(reverse) {
            return next-prev;
           }
           return prev-next;
        });
    }

    public static findEqual(listToSearch:Array<any>, element:any) {
        return listToSearch.includes(element);
    }
}